
const routes = [
  {
    path: '/ketua_kelas',
    component: () => import('layouts/Ketua_Kelas.vue'),
    children: [
      { path: '/ketua_kelas', component: () => import('pages/Home_Ketua_Kelas.vue') },
      { path: '/ketua_kelas/profile', component: () => import('pages/Profile_Ketua_Kelas.vue') },
      { path: '/ketua_kelas/profile/edit', component: () => import('pages/Profile_Edit_Ketua_Kelas.vue') },
      { path: '/ketua_kelas/form_kehadiran', component: () => import('pages/Form_Kehadiran_Dosen.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Login.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') },
      { path: 'login', component: () => import('pages/Login_BAAK.vue') }
      
    ]
  },
  {
    path: '/baak',
    component: () => import('layouts/BAAK.vue'),
    children: [
      { path: '/baak', component: () => import('pages/Home_BAAK.vue') },
      { path: '/baak/laporan', component: () => import('pages/Laporan_BAAK.vue') },
      { path: '/baak/jadwal', component: () => import('pages/Jadwal_BAAK.vue') },
      { path: '/baak/dosen', component: () => import('pages/Data_Akun_Dosen.vue') },
      { path: '/baak/ketua_kelas', component: () => import('pages/Data_Akun_Ketua_Kelas.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
